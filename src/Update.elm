module Update exposing (..)

import Model exposing (Product, Model, Msg(..))
import Decode exposing (decodeProducts)
import Http

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
      FetchProducts (Ok updatedProducts) ->
        ( { model | products = updatedProducts }, Cmd.none)
      FetchProducts (Err _) -> (model, Cmd.none)
      NoOp -> ( model, Cmd.none )
      SelectCategoryFilter state ->
            ( { model | filterCategory = state }
            , Cmd.none
            )

getProducts : Cmd Msg
getProducts =
  let
    url =
      "https://fathomless-journey-15644.herokuapp.com/api/products"

    request =
      Http.get url decodeProducts
  in
    Http.send FetchProducts request