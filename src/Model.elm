module Model exposing (..)

import Bootstrap.Dropdown as Dropdown

import Http

type alias Model = {
    products: List Product,
    filterCategory : Dropdown.State
}

type alias Product = {
    name: String
    , category: String
    , sold_out: Bool
    , under_sale: Bool
    , price: Int
    , sale_price: Int
    , sale_text: String
}

type Msg
    = NoOp
    | FetchProducts (Result Http.Error (List Product))
    | SelectCategoryFilter Dropdown.State