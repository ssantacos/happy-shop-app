module View exposing (..)

import Model exposing (Model)

import Html exposing (Html, text, div, h1, h3, img, button)
import Html.Attributes exposing (src, class)

import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Dropdown as Dropdown
import Bootstrap.Button as Button

import Model exposing (Product, Model, Msg(SelectCategoryFilter))
import Utils exposing (displayPrice, getCategories)

defaultImage: String
defaultImage = "/mockup.jpg"

heartImage: String
heartImage = "/heart.svg"

view : Model -> Html Msg
view model =
    div []
        [
          h1 [class "title"] [text "HAPPY SHOP"]
        , filterView model
        , productsView model.products
        ]

productsView : List Product -> Html Msg
productsView products =
    div [class "products"] (
          products |> List.map (productView)
        )

productView : Product -> Html Msg
productView product =
    Card.config [ Card.attrs [ class "product" ]]
      |> Card.header []
        [
          img [ src defaultImage, class "image" ] []
          , img [ src heartImage, class "product-card-heart" ] []
        ]
      |> Card.block []
        [
          Block.titleH6 [] [ text product.name ]
          , Block.text [] [ text product.category ]
          , Block.text [] [ text (product.price |> displayPrice) ]
        ]
      |> Card.view

categoryOptions : List String -> List (Dropdown.DropdownItem Msg)
categoryOptions categories =
  categories
  |> List.map (\category -> (Dropdown.buttonItem [] [ text category ]))

filterView : Model -> Html Msg
filterView model =
  div [class "filter"] [
    Dropdown.dropdown
        model.filterCategory
        { options = [ ]
        , toggleMsg = SelectCategoryFilter
        , toggleButton =
            Dropdown.toggle [ Button.light ] [ text "Choose category" ]
        , items = (model.products |> getCategories |> categoryOptions)
        }
    ]