module Decode exposing (..)

import Json.Decode as Decode
import Model exposing (Product)

decodeProduct : Decode.Decoder Product
decodeProduct =
  Decode.at ["data"] (decodeSingleProduct)

decodeProducts : Decode.Decoder (List Product)
decodeProducts =
  Decode.at ["data"] (Decode.list decodeSingleProduct)

decodeSingleProduct : Decode.Decoder Product
decodeSingleProduct =
  Decode.map7 Product
    (Decode.at ["name"] Decode.string)
    (Decode.at ["category"] Decode.string)
    (Decode.at ["sold_out"] Decode.bool)
    (Decode.at ["under_sale"] Decode.bool)
    (Decode.at ["price"] Decode.int)
    (Decode.at ["sale_price"] Decode.int)
    (Decode.at ["sale_text"] Decode.string)