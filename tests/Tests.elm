module Tests exposing (..)

import Test exposing (..)
import Expect

import Json.Decode as Decode
import Decode exposing (decodeProduct, decodeProducts)
import Utils
import Model exposing (Product)


product : String
product = """{
    \"data\": {
        \"name\": \"productName\",
        \"category\": \"productCategory\",
        \"sold_out\": false,
        \"under_sale\": false,
        \"price\": 10000,
        \"sale_price\": 5000,
        \"sale_text\": \"50% OFF\"
    }
}"""

all : Test
all =
    describe "Product Decoder"
        [ test "should decode basic product" <|
            \_ ->
                Decode.decodeString decodeProduct product
                |> Expect.equal (Result.Ok {
                    name = "productName"
                    , category = "productCategory"
                    , sold_out = False
                    , under_sale = False
                    , price = 10000
                    , sale_price = 5000
                    , sale_text = "50% OFF"
                })
        ]

products : String
products = """{
    \"data\": [{
        \"name\": \"productName\",
        \"category\": \"productCategory\",
        \"sold_out\": false,
        \"under_sale\": false,
        \"price\": 10000,
        \"sale_price\": 5000,
        \"sale_text\": \"50% OFF\"
    }]
}"""

allProduct : Test
allProduct =
    describe "Product List Decoder"
        [ test "should decode all basic product in the list" <|
            \_ ->
                Decode.decodeString decodeProducts products
                |> Expect.equal (Result.Ok [{
                    name = "productName"
                    , category = "productCategory"
                    , sold_out = False
                    , under_sale = False
                    , price = 10000
                    , sale_price = 5000
                    , sale_text = "50% OFF"
                }])
        ]

productLists : List Product
productLists = [
        Product "name" "category1" False False 100 50 "50% OFF"
        , Product "name" "category2" False False 100 50 "50% OFF"
        , Product "name" "category2" False False 100 50 "50% OFF"
    ] 

getCategories : Test
getCategories =
    describe "Get categories"
        [ test "should list all category from each product" <|
            \_ ->
                Utils.getCategories productLists
                |> Expect.equal ([
                    "category1"
                    , "category2"
                ])
        ]
